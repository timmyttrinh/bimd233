function flight(
  airline,
  number,
  origin,
  destination,
  depTime,
  arrivalTime,
  arrivalGate
) {
  this.airline = airline;
  this.number = number;
  this.origin = origin;
  this.destination = destination;
  this.depTime = depTime;
  this.arrivalTime = arrivalTime;
  this.arrivalGate = arrivalGate;

  var dT = new Date(this.depTime);
  var aT = new Date(this.arrivalTime);
  var duration = aT.getTime() - dT.getTime();

  var hoursLeft = duration % 3600000;
  var minutesLeft = hoursLeft % 60000;
  var secondsLeft = minutesLeft % 1000;

  var hours = (duration - hoursLeft) / 3600000;
  var minutes = (hoursLeft - minutesLeft) / 60000;
  var seconds = (minutesLeft - secondsLeft) / 1000;

  if (minutes < 10 && hours < 10) {
    this.duration = "0" + hours + ":" + "0" + minutes + ":" + seconds;
  } else if (minutes < 10) {
    this.duration = hours + ":" + "0" + minutes + ":" + seconds;
  } else if (hours < 10) {
    this.duration = "0" + hours + ":" + minutes + ":" + seconds;
  } else {
    this.duration = hours + ":" + minutes + ":" + seconds;
  }
}
var flight1 = new flight(
  "ASA1002",
  "A21N",
  "Reagan National",
  "San Fransico Intl",
  "Feburary 26 05:43:00",
  "Feburary 26 08:17:00",
  "GATE D9"
);

var flight2 = new flight(
  "ASA1024",
  "A320",
  "San Fransico Intl",
  "John F Kennedy Intl",
  "Feburary 26 01:47:00",
  "Feburary 26 09:48:00",
  "GATE 1"
);

var flight3 = new flight(
  "ASA1049",
  "B739",
  "Boston Logan Intl",
  "San Diego Intl",
  "Feburary 26 06:54:00",
  "Feburary 26 09:44:00",
  "GATE 26"
);

var flight4 = new flight(
  "ASA1063",
  "B739",
  "John F Kennedy Intl",
  "Seattle-Tacoma Intl ",
  "Feburary 26 07:28:00",
  "Feburary 26 10:07:00",
  "GATE A2"
);

var flight5 = new flight(
  "ASA1085",
  "B738",
  "Washington Dulles Intl",
  "Seattle-Tacoma Intl ",
  "Feburary 26 05:55:00",
  "Feburary 26 07:54:00",
  "GATE N19"
);

var allFlights = [flight1, flight2, flight3, flight4, flight5];

var el = document.getElementById("table");
el.innerHTML =
  "<th>Airline</th> <th>Number</th> <th>Origin</th> <th>Destination</th> <th>Departure Time</th> <th>Arrival Time</th> <th>Arrival Gate</th> <th>Flight Duration</th>";

for (var i = 0; i < allFlights.length; i++) {
  el.innerHTML +=
    "<tr><td>" +
    allFlights[i].airline +
    "</td><td>" +
    allFlights[i].number +
    "</td><td>" +
    allFlights[i].origin +
    "</td><td>" +
    allFlights[i].destination +
    "</td><td>" +
    allFlights[i].depTime +
    "</td><td>" +
    allFlights[i].arrivalTime +
    "</td><td>" +
    allFlights[i].arrivalGate +
    "</td><td>" +
    allFlights[i].duration +
    "</td></tr>";
}