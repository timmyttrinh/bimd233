var hiTemp = [82, 75, 69, 69, 68];
var loTemp = [55, 52, 52, 48, 51];
var allTemp = [82, 75, 69, 69, 68, 55, 52, 52, 48, 51];

var el = document.getElementById("avg");

function dayAvg(total, num) {
  return total + num;
}
el.innerHTML =
  "hi temp average: " +
  hiTemp.reduce(dayAvg) / hiTemp.length +
  "</br>" +
  "lo temp average: " +
  loTemp.reduce(dayAvg) / loTemp.length +
  "</br>" +
  "5 day temp average: " +
  allTemp.reduce(dayAvg) / allTemp.length;