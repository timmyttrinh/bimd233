var cars = [
  ["Mazda", "Nissan"],
  ["RX-7 (FD)", "Skyline GT-R (R34)"],
  ["1992", "1999"],
  ["$10,780", "$90,000"],
  ["Yellow", "Silver"]
];
var el = document.getElementById("Make");
el.textContent = cars[0][0];

var el = document.getElementById("Model");
el.textContent = cars[1][0];

var el = document.getElementById("Year");
el.textContent = cars[2][0];

var el = document.getElementById("Price");
el.textContent = cars[3][0];

var el = document.getElementById("Color");
el.textContent = cars[4][0];

var el = document.getElementById("Make2");
el.textContent = cars[0][1];

var el = document.getElementById("Model2");
el.textContent = cars[1][1];

var el = document.getElementById("Year2");
el.textContent = cars[2][1];

var el = document.getElementById("Price2");
el.textContent = cars[3][1];

var el = document.getElementById("Color2");
el.textContent = cars[4][1];