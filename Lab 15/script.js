var stocks = [
  {
    name: "Microsoft",
    cap: "$381.7 B",
    sales: "$86.8 B",
    profit: "$22.1 B",
    employees: "128,000"
  },
  {
    name: "Symetra Financial",
    cap: "$2.7 B",
    sales: "$2.2 B",
    profit: "$254.4 M",
    employees: "1,400"
  },
  {
    name: "Micron Technology",
    cap: "$37.6 B",
    sales: "$16.4 B",
    profit: "$3.0 B",
    employees: "30,400"
  },
  {
    name: "F5 Networks",
    cap: "$9.5 B",
    sales: "$1.7 B",
    profit: "$311.2 M",
    employees: "3,834"
  },
  {
    name: "Expedia",
    cap: "$10.8 B",
    sales: "$5.8 B",
    profit: "$398.1 M",
    employees: "18,210"
  },
  {
    name: "Nautilus",
    cap: "$476 M",
    sales: "$274.4 M",
    profit: "$18.8 M",
    employees: "340"
  },
  {
    name: "Heritage Financial",
    cap: "$531 M",
    sales: "$137.6 M",
    profit: "$21 M",
    employees: "748"
  },
  {
    name: "Cascade Microtech",
    cap: "$239 M",
    sales: "$136 M",
    profit: "$9.9 M",
    employees: "449"
  },
  {
    name: "Nike",
    cap: "$83.1 B",
    sales: "$27.8 B",
    profit: "$2.7 B",
    employees: "56,500"
  },
  {
    name: "Alaska Air Group",
    cap: "$7.9 B",
    sales: "$5.4 B",
    profit: "$605 M",
    employees: "13,952"
  }
];

var el = document.getElementById("stockTable");

el.innerHTML =
  "<th>Company Name</th> <th>Market Cap</th> <th>Sales</th> <th>Profit</th> <th># of Employees</th>";

function myFunction(item) {
  el.innerHTML +=
    "<tr><td>" +
    item.name +
    "</td><td>" +
    item.cap +
    "</td><td>" +
    item.sales +
    "</td><td>" +
    item.profit +
    "</td><td>" +
    item.employees +
    "</td><td>";
}