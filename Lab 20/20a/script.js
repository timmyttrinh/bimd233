var myUrl = "https://api.coindesk.com/v1/bpi/currentprice";

var arr = [];

var graph = Morris.Line({
  element: "graph",
  data: arr,
  xkey: "time",
  ykeys: ["price"],
  ymin: "auto",
  ymax: "auto",
  labels: ["amount"],
  parseTime:false,
  hideHover:true
});
var code = null;
function update(code) {
  $.ajax({
    url: myUrl+"/"+code+".json",
    success: function(data) {
      var bc = JSON.parse(data);
      var p = bc.bpi[code].rate_float;
      var t =bc.time.updated;
      var n ={price:p, time:t};
      updateData(n);
      graph.setData(arr);
    }
  });
}
function updateData(n) {
  if (arr.length >= 10) {
    arr.shift();
  }
  arr.push(n);
}

$(document).ready(function() {
  $("#hit-me").click(function() {
    var newCode = $("#curency").val();
    if (code != newCode) {
      arr = [];
      code = newCode;
    }
    setInterval(function() { update(code); }, 10000);
  });
});